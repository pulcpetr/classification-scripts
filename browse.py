#!/usr/bin/env python3
import argparse

from IPython import embed

from config import API_BASE
from oauth_client import Client


def parse_args():
    parser = argparse.ArgumentParser(description='Open current results in IPython')
    parser.add_argument('course', help='course code (MI-PYT)')
    parser.add_argument('--group', default='ALL', help='get specific group only')
    parser.add_argument('--semester', help='semester code (Byyn)', default='')

    return parser.parse_args()


def run():
    args = parse_args()

    client = Client()

    semester = ''
    if args.semester:
        semester = '?semester={}'.format(args.semester)

    url = '{}courses/{}/group/{}/student-classifications{}'. \
        format(API_BASE, args.course, args.group, semester)
    response = client.get(url)

    if not response.ok:
        print(response, response.content)
        return

    data = response.json()

    print('''
  IPython starting, parsed response is stored in variable "data".

  To get, for example, email addresses of students that have less then 10 points in column "exam":
  [u['email'] for u in data if u['classificationMap'].get('exam', 0) < 10]
    ''')

    embed()


if __name__ == '__main__':
    run()
