MY_SERVER = 'localhost'
MY_PORT = 8080

REDIRECT_URI = 'http://{}:{}'.format(MY_SERVER, MY_PORT)
AUTHORIZE_URL = 'https://auth.fit.cvut.cz/oauth/authorize'
TOKEN_URL = 'https://auth.fit.cvut.cz/oauth/token'

API_BASE = 'https://grades.fit.cvut.cz/api/v1/public/'
