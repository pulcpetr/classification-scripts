#!/usr/bin/env python3
from http import server
from pprint import pprint
from urllib.parse import parse_qs

from requests_oauthlib import OAuth2Session

from config import *
from secrets import CLIENT_ID, CLIENT_SECRET

CODE = ''


class CodeHandler(server.BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        global CODE
        CODE = parse_qs(self.path[2:])['code'][0]
        self._set_headers()
        self.wfile.write(b'<html><body>This window may be now safely closed.</body></html>')


# init OAuth client
oauth = OAuth2Session(CLIENT_ID, redirect_uri=REDIRECT_URI)
auth_url, _ = oauth.authorization_url(AUTHORIZE_URL)

print('Go to: {}'.format(auth_url))

# init server and get code
server.HTTPServer((MY_SERVER, MY_PORT), CodeHandler).handle_request()

token = oauth.fetch_token(TOKEN_URL, CODE, client_id=CLIENT_ID, client_secret=CLIENT_SECRET)
pprint(token)
