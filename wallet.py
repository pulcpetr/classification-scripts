#!/usr/bin/env python3
import argparse

from config import API_BASE
from dto import DTO
from oauth_client import Client


def parse_args():
    parser = argparse.ArgumentParser(description='Split activity points from a wallet')
    parser.add_argument('course', help='course code (MI-PYT)')
    parser.add_argument('group', help='group code')
    parser.add_argument('total', type=int, help='total of points in wallet')
    parser.add_argument('activity', help='activity column (data source)')
    parser.add_argument('points', help='points column (data destination)')
    parser.add_argument('--semester', help='semester code (Byyn)', default='')

    return parser.parse_args()


def run():
    args = parse_args()

    client = Client()

    semester = ''
    if args.semester:
        semester = '?semester={}'.format(args.semester)

    url = '{}courses/{}/group/{}/student-classifications{}'. \
        format(API_BASE, args.course, args.group, semester)
    response = client.get(url)

    if not response.ok:
        print(response, response.content)
        return

    data = response.json()
    total = sum(s['classificationMap'].get(args.activity, 0) for s in data)

    dto = DTO()
    for student in data:
        dto.add(student['username'],
                args.points,
                student['classificationMap'].get(args.activity, 0) * args.total / total)

    ret = client.put('{}courses/{}/student-classifications'.format(API_BASE, args.course), dto)
    print(ret)


if __name__ == '__main__':
    run()
