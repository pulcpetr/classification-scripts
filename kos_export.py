#!/usr/bin/env python3
import argparse

from config import API_BASE
from oauth_client import Client


def parse_args():
    parser = argparse.ArgumentParser(description='Export JavaScript for import to KOS')
    parser.add_argument('course', help='course code (MI-PYT)')
    parser.add_argument('column', help='result column identification')
    parser.add_argument('--group', default='ALL', help='get specific group only')
    parser.add_argument('--semester', help='semester code (Byyn)', default='')

    return parser.parse_args()


def run():
    args = parse_args()

    classification = Client()

    semester = ''
    if args.semester:
        semester = f'?semester={args.semester}'

    url = f'{API_BASE}courses/{args.course}/group/{args.group}/student-classifications{semester}'. \
        format(API_BASE, args.course, args.group, semester)
    data = classification.get(url)

    if not data.ok:
        print(data, data.content)
        return

    print('''
students = Object.fromEntries(Array.from(document.getElementsByClassName("row-headline")).map(x => [x.querySelector('[data-testid="username"]').innerText.toLowerCase(), x.querySelectorAll('button.grade')[1]]))
''')
    for e in data.json():
        class_map = e['classificationMap']
        if args.column in class_map:
            if type(class_map[args.column]['value']) is bool and class_map[args.column]['value']:
                print(f'students["{e["username"]}"].click()')


if __name__ == '__main__':
    run()
