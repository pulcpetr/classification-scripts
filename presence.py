#!/usr/bin/env python3
import argparse

from dto import DTO
from oauth_client import Client
from config import API_BASE


def parse_args():
    parser = argparse.ArgumentParser(description='Mark presence for usernames listed in file.')
    parser.add_argument('course', help='course code (MI-PYT)')
    parser.add_argument('column', help='column identification')
    parser.add_argument('file', help='file with usernames')

    return parser.parse_args()


def run():
    args = parse_args()

    classification = Client()
    url = '{}courses/{}/group/ALL/student-classifications'. \
        format(API_BASE, args.course)
    data = classification.get(url)
    students = set(e['username'] for e in data.json())

    dto = DTO()

    with open(args.file) as file:
        for row in file:
            username = row.strip()
            if username in students:
                dto.add(row.strip(), args.column, True)
            elif username:
                print('Student {} does not exist'.format(username))

    url = '{}courses/{}/student-classifications'.format(API_BASE, args.course)
    ret = classification.put(url, dto)
    print(ret)


if __name__ == '__main__':
    run()
