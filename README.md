# Sada skriptů pro práci s aplikací Klasifikace

Kdož chceš dávkově klasifikaci udělovat, zbystři!

Nenásob zbytečně práci a raději přidej ruku k dílu...

Jiný jazyk? Jiný předmět? Jiné zvyky? Uprav! A šiř! :heart:

## Usage

Nejprve je třeba získat Client ID a Client Secret key.

K tomu je třeba se přihlásit na https://auth.fit.cvut.cz/manager/index.xhtml
založit tam projekt, aktivovat scope cvut:grades:user-write a cvut:grades:user-read (pod službou Klasifikace)
a přidat webovou aplikaci s přesměrováním na `http://localhost`.

Zobrazené Client ID a Client Secret key zkopírovat do souboru secrets.py
(podle vzoru secrets.sample.py)

### presence

Očekává: `kód předmětu` `identifikátor sloupce` `soubor s username v řádcích`

Vyplní prezenci (zaškrtne políčko).

### score

Očekává: `kód předmětu` `identifikátor sloupce` `soubor s username:skóre v rádcích`

Rozdělovník je možné nastavit přepínačem `-d`

Vyplní numerické hodnoty skóre.

### kos_export

Očekává: `kód předmětu` `identifikátor sloupce`

Přepínačem `--group` je možné zvolit skupinu, výchozí je 'ALL'.

Přepínačem `--semester` je možné zvolit semestr.

Vygeneruje JavaScript pro import klasifikovaných zápočtů (pokud je sloupec typu text) nebo zápočtů (pokud je sloupec typu boolean) do KOSu.

>Není otestované pro zápis známek ze zkoušky.

### wallet

Očekává: `kód předmětu` `identifikátor skupiny` `počet bodů v měšci` `identifikátor sloupce s plusíky (vstup)` `identifikátor sloupce s bonusovými body (výstup)`

Přepínačem `--semester` je možné zvolit semestr.

Rozdělí podle informaci ve sloupci s plusíky body z měšce do sloupce s reálnými body.

### browse

Očekává: `kód předmětu`

Přepínačem `--group` je možné zvolit skupinu, výchozí je 'ALL'.

Přepínačem `--semester` je možné zvolit semestr.

Otevře IPython s informacemi o klasifikaci v proměnné `data` pro libovolné zkoumání.

### student_list

Očekává: `kód předmětu`

Přepínačem `--group` je možné zvolit skupinu, výchozí je 'ALL'.

Přepínačem `--semester` je možné zvolit semestr.

Přepínačem `--out` je možné data uložit ve formátu pickle do zadaného souboru, jinak je výstup ve formátu CSV bez hlavičky.

Přepínačem `--fields` je možné zvolit pole pro výstup, výchozí hodnota je maximální smysluplný výstup.

Vypíše nebo uloží seznam studentů.

### get_webapp_token

Stačí spustit.

Po kliku na odkaz a přihášení skript vypíše kompletní oauth token objekt.

### oauth_client

Knihovna pro budoucí použití.

```python
from oauth_client import Client

client = Client()

client.get('tajny resource')
client.post('jeste tajnejsi', '{"dulezita": "JSON data!"}')
client.put('jeste tajnejsi', '{"dulezita": "JSON data!"}')
```

Rychlé poznámky:
- při prvním spuštění nebo po smazání token.p se chová stejně jako get_webapp_token
- ukládá token objekt do token.p (zapiklený) pro další spuštětní
- používá auto_refresh
- přidává automaticky hlavičku Content-type: application/json k post a put
