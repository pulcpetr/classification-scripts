#!/usr/bin/env python3
import argparse

from config import API_BASE
from dto import DTO
from oauth_client import Client


def parse_args():
    parser = argparse.ArgumentParser(description='Mark presence for usernames listed in file.')
    parser.add_argument('course', help='course code (MI-PYT)')
    parser.add_argument('column', help='column identification')
    parser.add_argument('--delimiter', '-d', default=':', help='username and score delimiter')
    parser.add_argument('--force', '-f', action='store_true', help='overwrite higher scores in grades')
    parser.add_argument('--yes', '-y', action='store_true', help='skip prompt')
    parser.add_argument('file', help='file with usernames and scores')

    return parser.parse_args()


def run():
    args = parse_args()
    if len(args.delimiter) != 1:
        raise RuntimeError('Delimiter has to be one character long.')

    classification = Client()
    url = '{}courses/{}/group/ALL/student-classifications'. \
        format(API_BASE, args.course)
    data = classification.get(url)
    student_scores = {e['username']: e['classificationMap'].get(args.column, {'value': 0})['value'] for e in
                      data.json()}

    dto = DTO()

    with open(args.file) as file:
        for row in file:
            if row.strip():
                username, score = row.strip().split(args.delimiter)
                if username in student_scores:
                    score = float(score)
                    if not args.force and student_scores[username] > score:
                        print('Student {} has higher score already'.format(username))
                    elif student_scores[username] < score:
                        dto.add(username, args.column, score)
                elif username:
                    print('Student {} does not exist'.format(username))

    url = '{}courses/{}/student-classifications'.format(API_BASE, args.course)
    print(dto)
    if not args.yes:
        input('Submit?')
    ret = classification.put(url, dto)
    print(ret)


if __name__ == '__main__':
    run()
