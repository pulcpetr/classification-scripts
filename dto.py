class DTO(list):
    def add(self, username: str, identifier: str, value, note: str = None):
        item = {'studentUsername': username,
                'classificationIdentifier': identifier,
                'value': value}
        if note:
            item['note'] = note

        self.append(item)
