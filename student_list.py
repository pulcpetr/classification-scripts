#!/usr/bin/env python3
import argparse
import pickle

from config import API_BASE
from oauth_client import Client


def parse_args():
    parser = argparse.ArgumentParser(description='Get list of students')
    parser.add_argument('course', help='course code (MI-PYT)')
    parser.add_argument('--group', default='ALL', help='get specific group only')
    parser.add_argument('--semester', help='semester code (Byyn)', default='')
    parser.add_argument('--out', '-o', help='pickled output file')
    parser.add_argument('--fields', help='output fields', default='personalNumber,email,username,firstName,lastName')

    return parser.parse_args()


def run():
    args = parse_args()

    classification = Client()

    semester = ''
    if args.semester:
        semester = '?semester={}'.format(args.semester)

    url = '{}courses/{}/group/{}/student-classifications{}'. \
        format(API_BASE, args.course, args.group, semester)
    data = classification.get(url)

    if not data.ok:
        print(data, data.content)
        return

    fields = args.fields.split(',')

    if args.out:
        data_out = []
        for e in data.json():
            data_out.append({k: v for k, v in e.items() if k in fields})
        pickle.dump(data_out, open(args.out, 'wb'))
        return

    for e in data.json():
        print(','.join(str(e[k]) for k in fields))


if __name__ == '__main__':
    run()
