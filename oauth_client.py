import pickle
from http import server
from os import path
from urllib.parse import parse_qs

from oauthlib.oauth2 import BackendApplicationClient
from requests_oauthlib import OAuth2Session

from config import *
from secrets import CLIENT_ID, CLIENT_SECRET, WORKFLOW

TOKEN_FILE = path.join(path.dirname(__file__), 'token.p')

CLIENT_KWARGS = {'client_id': CLIENT_ID, 'client_secret': CLIENT_SECRET}

CODE = ''


class CodeHandler(server.BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def log_message(self, _, *args):
        pass

    def do_GET(self):
        global CODE
        CODE = parse_qs(self.path[2:])['code'][0]
        self._set_headers()
        self.wfile.write(b'<html><body>This window may be now safely closed.</body></html>')


class Client:
    client = None

    def __init__(self):
        if WORKFLOW != 'BACKEND':
            try:
                self.load_token()
                return
            except FileNotFoundError:
                pass
        self.new_token()

    def load_token(self):
        token = pickle.load(open(TOKEN_FILE, 'rb'))
        self.new_client(token)

    def save_token(self, token):
        pickle.dump(token, open(TOKEN_FILE, 'wb'))
        self.new_client(token)

    def new_token(self):
        if WORKFLOW == 'WEB':
            oauth = OAuth2Session(CLIENT_ID, redirect_uri=REDIRECT_URI)
            auth_url, _ = oauth.authorization_url(AUTHORIZE_URL)

            print('Go to: {}'.format(auth_url))

            # init server and get code
            server.HTTPServer((MY_SERVER, MY_PORT), CodeHandler).handle_request()

            token = oauth.fetch_token(TOKEN_URL, CODE, client_id=CLIENT_ID, client_secret=CLIENT_SECRET)
            self.save_token(token)
        elif WORKFLOW == 'BACKEND':
            oauth = OAuth2Session(client=BackendApplicationClient(client_id=CLIENT_ID))
            token = oauth.fetch_token(TOKEN_URL, client_id=CLIENT_ID, client_secret=CLIENT_SECRET)
            self.new_client(token)
        else:
            raise NotImplementedError('Unknown OAuth workflow, only WEB and BACKEND supported.')

    def new_client(self, token):
        self.client = OAuth2Session(CLIENT_ID, token=token,
                                    auto_refresh_url=TOKEN_URL,
                                    auto_refresh_kwargs=CLIENT_KWARGS,
                                    token_updater=self.save_token)

    def get(self, url, **kwargs):
        kwargs.update(CLIENT_KWARGS)
        return self.client.get(url, **kwargs)

    def post(self, url, json, **kwargs):
        kwargs.update(CLIENT_KWARGS)
        return self.client.post(url, json=json, headers={'Content-Type': 'application/json'}, **kwargs)

    def put(self, url, json, **kwargs):
        kwargs.update(CLIENT_KWARGS)
        return self.client.put(url, json=json, headers={'Content-Type': 'application/json'}, **kwargs)
